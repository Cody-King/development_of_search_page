import Taro, { Component } from '@tarojs/taro';
// 引入需要的组件
import { View, Input, Button, Navigator, ScrollView } from '@tarojs/components';
import './index.scss';
// 引入axios用于发送请求
import axios from 'axios';
export default class Index extends Component {
  //初始化防抖计时器
  timeId = -1;
  state = {
    //输入框的值
    iptValue: '',
    //数据列表
    tipList: [],
    //是否显示按钮
    isShow: false,
    //当前页数
    current: 1,
    //总页数
    pages: 1
  };
  //手指滑动触发下拉刷新和上拉加载
  touchmove = () => {
    console.log('进行了滑动操作');
  };
  //监听页面完成  默认加载后台给的默认数据
  componentDidMount() {
    this.getListData();
  }
  // 输入框发生变化
  inputChange = e => {
    //获取输入框的值
    const { value } = e.target;
    this.setState({
      iptValue: value
    });
    // 校验输入框非空
    if (!value.trim()) {
      //为空返回 重置输入框 隐藏取消按钮
      return this.setState({
        tipList: [],
        isFocus: false
      });
    } else {
      Taro.showLoading({
        title: '加载中'
      });
      this.setState({
        //输入框有值  显示取消按钮
        isFocus: true
      });
      //清除防抖定时器
      clearTimeout(this.timeId);
      //校验成功获取数据
      this.timeId = setTimeout(() => {
        const { current } = this.state;
        this.getListData(current, value);
      }, 500);
    }
  };
  //封装获取数据函数
  async getListData(current, value) {
    const res = await axios.post(
      'http://2l89512r05.zicp.vip/cloud-service/cross/test',
      {
        pageNum: current, // 页码
        keyword: value
      }
    );
    console.log(res);
    if (res.data.code === 0) {
      //请求成功
      this.setState({
        tipList: res.data.result.records,
        current: res.data.result.current,
        pages: res.data.result.pages
      });
      //请求数据成功提示信息
      Taro.showToast({
        title: '加载完成',
        icon: 'success'
      });
      //关闭下拉刷新
      Taro.stopPullDownRefresh();
    } else {
      //请求失败
      alert('请求数据失败');
    }
  }

  // 点击取消按钮 清空输入框的值 隐藏取消按钮
  handleCancel = () => {
    this.setState({
      iptValue: '',
      tipList: [],
      isFocus: false
    });
  };
  // 渲染列表
  // rendTips=()=>{
  // console.log(this.state.tipList)
  // const {tipList} = this.state
  //  tipList.map(item=>{
  //   return <navigator key={item.id}>1111{item.text}</navigator>
  // })
  // }

  //下拉刷新
  onPullDownRefresh() {
    //温馨提示
    Taro.showToast({
      title: '刷新中',
      icon: 'loading'
    });
    //重新加载数据
    const { current, iptValue } = this.state;
    this.getListData(current, iptValue);
  }
  //页面上拉触底事件的处理函数
  onReachBottom() {
    Taro.showToast({
      title: '上拉加载更多',
      icon: 'loading'
    });
    const { current, pages, iptValue } = this.state;
    if (current === pages - 1) {
      //没有数据了
      Taro.showToast({
        title: '这是最后一页了'
      });
    }
    //有下一页，让当前页加一，重新获取数据
    this.state.current++;
    this.getListData(this.state.current, iptValue);
    // 滚到顶部
    Taro.pageScrollTo({
      scrollTop: 0,
      duration: 500
    });
  }
  config = {
    navigationBarTitleText: '首页'
  };

  render() {
    const { iptValue, isFocus, tipList } = this.state;
    return (
      <View>
        {/* 搜索框 */}
        <View className='search'>
          <Input
            placeholder='请输入您要搜索的内容'
            value={iptValue}
            onChange={this.inputChange}
          />
          <Button hidden={!isFocus} onClick={this.handleCancel}>
            取消
          </Button>
        </View>
        <ScrollView
          className='dragChange'
          onTouchMove={this.touchmove}
          scrollY
          scrollWithAnimation
        >
          {/* 搜索数据列表 */}
          <View className='search_list'>
            {/* {this.rendTips()} */}
            {tipList.map(item => {
              return (
                <Navigator className='navg' key={item.id}>
                  {item.text}
                </Navigator>
              );
            })}
          </View>
        </ScrollView>
      </View>
    );
  }
}
